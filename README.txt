
-- SUMMARY --

Hi5 is a very minimal HTML5 base theme that wraps header, footer, content, menus
and blocks in appropriate tags. Plus, it adds a simple mechanism to link to
external scripts and stylesheets.

Hi5 implements a rather extreme CSS reset: Apart from providing some fairly
standard CSS rules to normalize HTML display across different browsers and
platforms, it removes all core and module stylesheets that are not explicitly
whitelisted by the child theme.

Hi5's homepage is on drupal.org, of course:
  http://drupal.org/project/hi5

Please file any issues you might have with Hi5 there, too:
  http://drupal.org/project/issues/hi5


-- CSS RESET --

Modules can be excluded from stylesheet removal by adding them to the
'css whitelist' array in the theme .info file.

  ; module css whitelist
  css whitelist[] = admin
  css whitelist[] = fancybox

Hi5 literally gives you a blank canvas (pun intended) for advanced and arty
HTML5 themes - and therefore it is highly recommended, though not strictly
required, to use a dedicated administration theme such as Rubik or Seven with
Hi5. Another obvious recommendation is to use either Admin or Admin-Menu module.


-- EXTERNAL ASSET SUPPORT --

To make a child theme use an external JavaScript file, such as Remy Sharp's
html5shim.js (http://remysharp.com/2009/01/07/html5-enabling-script/), it only
needs to be added in the .info file:

  ; external javascript
  external scripts[] = http://html5shim.googlecode.com/svn/trunk/html5.js

Similarly, external stylesheets, for example to enable a webfont like Kaffeesatz
(http://www.yanone.de/typedesign/kaffeesatz/) for a theme, can easily be added:

  ; external stylesheets
  external stylesheets[] = http://fonts.googleapis.com/css?family=Yanone+Kaffeesatz


-- DRUPAL HTML5 SUPPORT --

Drupal core and almost all contributed modules produce valid XHTML: All that is
needed to make it output valid HTML5 is to change the doctype. Hi5 additionally
applies <header>, <footer>, <nav> and other HTML5 tags where semantically
appropriate.

Please note that Hi5 does not use XHTML5, because it is a) widely discouraged
(e.g. http://oli.jp/2009/html5-structure4/#html5-xhtml5) and b) impossible to
achieve in Drupal 6 without a core patch (see http://drupal.org/node/451304).

If you need help getting started with HTML5, head over to http://html5demos.com
or http://html5doctor.com. To improve backward compatibility and to avoid vendor
specific CSS (e.g. -moz-border-radius), consider using javascripts like
Modernizr (http://www.modernizr.com/) and eCSStender (http://ecsstender.org/).


-- CONTACT --

Maintainer:
  Daniel Dembach (dmbch) - http://drupal.org/user/159765

Development of this theme is happening mainly on Github:
  https://github.com/dmbch/hi5
