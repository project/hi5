<?php

/**
 * @file
 * The main Hi5 theme file.
 *
 * Hi5 is very loosely based on Development Seed's Tao theme:
 * http://code.developmentseed.org/tao/node/157
 */

/**
 * Implementation of hook_theme().
 */
function hi5_theme() {
  $path = drupal_get_path('theme', 'hi5') .'/tpl';

  $items = array();
  $items['block'] =
  $items['box'] =
  $items['comment'] =
  $items['node'] = array(
    'template' => 'object',
    'path' => $path,
  );
  $items['maintenance_page'] = array(
    'template' => 'page',
    'path' => $path,
  );

  return $items;
}


/**
 * Return a breadcrumb trail.
 */
function hi5_breadcrumb($breadcrumb) {
  return (!empty($breadcrumb)) ? implode(' » ', $breadcrumb) : NULL;
}

/**
 * Return a themed help message.
 */
function hi5_help() {
  return ($help = menu_get_active_help()) ? $help : NULL;
}


/**
 * Implementation of hook_preprocess_block().
 */
function hi5_preprocess_block(&$variables) {
  $variables['attributes'] = array();
  $variables['attributes']['id'] = "block-{$variables['block']->module}-{$variables['block']->delta}";
  $variables['attributes']['class'] = "block block-{$variables['block']->module}";
  $variables['element'] = 'div';
  $variables['title'] = !empty($variables['block']->subject) ? $variables['block']->subject : '';
  $variables['content'] = $variables['block']->content;
}


/**
 * Implementation of hook_preprocess_box().
 */
function hi5_preprocess_box(&$variables) {
  $variables['attributes'] = array('class' => 'box');
  $variables['element'] = 'div';
}


/**
 * Implementation of hook_preprocess_comment().
 */
function hi5_preprocess_comment(&$variables) {
  $variables['attributes'] = array('class' => 'comment');
  $variables['element'] = 'article';
}


/**
 * Implementation of hook_preprocess_maintenance_page().
 */
function hi5_preprocess_maintenance_page(&$variables) {
  hi5_preprocess_page($variables);

  $variables['breadcrumb'] =
  $variables['primary_links'] =
  $variables['secondary_links'] =
  $variables['tabs'] = NULL;
}


/**
 * Implementation of hook_preprocess_node().
 */
function hi5_preprocess_node(&$variables) {
  $variables['attributes'] = array();
  $variables['attributes']['id'] = "node-{$variables['node']->nid}";
  $variables['attributes']['class'] = "node node-{$variables['node']->type}";
  $variables['element'] = 'article';
}


/**
 * Implementation of hook_preprocess_page().
 */
function hi5_preprocess_page(&$variables) {
  global $theme_key;

  $variables['attributes'] = array();
  $variables['attributes']['class'] = $variables['body_classes'];
  $variables['attributes']['class'] .= ' '. $theme_key;

  // Remove duplicate content-type header -- see http://drupal.org/node/451304
  $variables['headers'] = preg_replace('/<meta http-equiv=\"Content-Type\"[^>]*>\n/', '', $variables['head']);

  // Add links to external assets
  $variables['assets'] = _hi5_assets($theme_key);

  // Get minimal styles
  $variables['styles'] = _hi5_styles($theme_key);
}


/**
 * Generate links to external assets (CSS and JS).
 */
function _hi5_assets($theme_key, $type = NULL) {
  static $assets = array();

  if (empty($assets)) {
    $themes = list_themes();
    $theme = $themes[$theme_key];

    // Add external scripts
    $scripts = (is_array($theme->info['external scripts'])) ? $theme->info['external scripts'] : array();
    $assets['scripts'] = '';
    foreach ($scripts as $url) {
      $assets['scripts'] .= "<script type=\"text/javascript\" src=\"". htmlspecialchars($url) ."\"></script>\n";
    }

    // Add external stylesheets
    $media = (is_array($theme->info['external stylesheets'])) ? $theme->info['external stylesheets'] : array();
    $assets['styles'] = '';
    foreach ($media as $medium => $styles) {
      foreach ($styles as $url) {
        $assets['styles'] .= "<link type=\"text/css\" rel=\"stylesheet\" media=\"$medium\" href=\"". htmlspecialchars($url) ."\" />\n";
      }
    }
  }

  return ($type) ? $assets[$type] : implode('', $assets);
}


/**
 * Remove most stylesheets.
 */
function _hi5_styles($theme_key) {
  static $styles = '';

  if (empty($styles)) {
    $themes = list_themes();
    $theme = $themes[$theme_key];

    $css = drupal_add_css();

    // Add path to current theme (plus base theme[s]) to whitelist.
    $whitelist = _hi5_theme_paths($theme_key);

    // Add paths for specified modules to whitelist.
    $exceptions = (is_array($theme->info['css whitelist'])) ? $theme->info['css whitelist'] : array();
    foreach ($exceptions as $exception) {
      if (module_exists($exception)) {
        $whitelist[] = drupal_get_path('module', $exception) .'/*';
      }
    }

    // Implode whitelist array to use with drupal_match_path()
    $whitelist = implode("\n", $whitelist);

    // Actually remove css files not found in whitelist.
    foreach ($css as $media => $types) {
      foreach ($types as $type => $paths) {
        foreach (array_keys($paths) as $path) {
          if (!drupal_match_path($path, $whitelist)) {
            unset($css[$media][$type][$path]);
          }
        }
      }
    }

    // Trigger CSS aggregation, get HTML
    $styles = drupal_get_css($css);
  }

  return $styles;
}


/**
 * (Recursively) get current theme and base theme paths.
 */
function _hi5_theme_paths($theme_key) {
  $themes = list_themes();
  $theme = $themes[$theme_key];

  $paths = array(drupal_get_path('theme', $theme_key) .'/*');
  if (isset($theme->info['base theme'])) {
    $paths = array_merge($paths, _hi5_theme_paths($theme->info['base theme']));
  }
  return $paths;
}
