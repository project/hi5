<?php // $Id$ ?>
<!doctype html>
<html lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>">

<head>
<?php print $headers; ?>
<?php print $assets; ?>
<?php print $styles; ?>
<title><?php print $head_title; ?></title>
</head>

<body<?php print drupal_attributes($attributes) ?>>

<?php if (!empty($admin)) print $admin ?>

<header>
  <?php if ($site_name) : ?>
    <h1><a href="<?php print $front_page ?>"><?php print $site_name ?></a></h1>
  <?php endif; ?>
  <?php if ($header) print $header ?>
</header>

<div id="wrap">
  <div id='main'>
    <?php if ($breadcrumb) : ?><nav id="breadcrumb"><?php print $breadcrumb; ?></nav><?php endif; ?>
    <?php if ($tabs) : ?><nav id="tabs"><?php print $tabs ?></nav><?php endif; ?>
    <?php if ($help) : ?><div id="help"><?php print $help; ?></div><?php endif; ?>
    <?php print $content ?>
  </div>

  <aside id="sidebar"><?php if ($aside) print $aside ?></aside>
</div>

<?php if ($primary_links || $secondary_links) : ?>
<nav id="menu">
  <?php if ($primary_links) : ?>
    <?php print theme('links', $primary_links, array('class' => 'links primary-links')) ?>
  <?php endif; ?>
  <?php if ($secondary_links) : ?>
    <?php print theme('links', $secondary_links, array('class' => 'links secondary-links')) ?>
  <?php endif; ?>
</nav>
<?php endif; ?>

<?php if ($footer || $footer_message) : ?>
<footer>
  <?php print $footer ?>
  <?php print $footer_message ?>
</footer>
<?php endif; ?>

<?php if ($show_messages && $messages) : print $messages; endif; ?>

<?php print $scripts ?>
<?php print $closure ?>

</body>

</html>
