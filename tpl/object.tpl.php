<?php // $Id$ ?>
<<?php print $element . drupal_attributes($attributes) ?>>

  <?php if (!empty($title)) : ?><h2><?php print $title ?></h2><?php endif; ?>

  <?php if (!empty($submitted)): ?>
    <div class="submitted">
      <?php print $submitted ?>
    </div>
  <?php endif; ?>

  <?php if (!empty($content)) : ?>
    <div class="content">
      <?php print $content ?>
    </div>
  <?php endif; ?>

  <?php if (!empty($links)) : ?>
    <div class="links">
      <?php print $links ?>
    </div>
  <?php endif; ?>

</<?php print $element ?>>
